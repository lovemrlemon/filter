This is one of the assignments I did in Spring 2020 for CS50.

Please find the details for this project [here](https://cs50.harvard.edu/x/2021/psets/4/filter/more/)

The main focus of this project was to learn memory management in C, as well as working with 2D arrays.