#include "helpers.h"
#include <stdio.h>
#include <stdlib.h>
#include <math.h>

int check_interval(int x);


// Convert image to grayscale
void grayscale(int height, int width, RGBTRIPLE image[height][width])
{
    for (int i = 0; i < height; i++)
    {
        float r, g, b;
        float avg;
        for (int j = 0; j < width; j++)
        {
            r = image[i][j].rgbtRed;
            g = image[i][j].rgbtGreen;
            b = image[i][j].rgbtBlue;
            avg = (int) round((r + g + b) / 3);
            
            image[i][j].rgbtRed = avg;
            image[i][j].rgbtGreen = avg;
            image[i][j].rgbtBlue = avg;
        }
    }
    return;
}


// Reflect image horizontally
void reflect(int height, int width, RGBTRIPLE image[height][width])
{   
    
    RGBTRIPLE temp;
    for (int i = 0; i < height; i++)
    {
        for (int j = 0; j < width / 2; j++)
        {   
            //swap the two pixels
            temp = image[i][j];
            image[i][j] = image[i][width - 1 - j];
            image[i][width - 1 - j] = temp;
        }
    }
    return;
}

// Blur image
void blur(int height, int width, RGBTRIPLE image[height][width])
{   
    // temporary array to keep the edited values.
    RGBTRIPLE temp_image[height][width];

    for (int i = 0; i < height; i++)
    {
        for (int j = 0; j < width; j++)
        {   
            float sum_r = 0, sum_g = 0, sum_b = 0;
            float counter = 0;
            
            for (int m = i - 1; m <= i + 1; m++)
            {
                // check for edges
                if ((m < 0) || (m > height - 1))
                {
                    continue;
                }
                for (int n = j - 1; n <= j + 1; n++)
                {
                    // check for edges
                    if ((n < 0) || (n > width - 1))
                    {
                        continue;
                    }
                    sum_r += image[m][n].rgbtRed;
                    sum_g += image[m][n].rgbtGreen;
                    sum_b += image[m][n].rgbtBlue;
                    counter ++;
                }
            }
            // calculate the avg for each r g b value.
            temp_image[i][j].rgbtRed = (int) round(sum_r / counter);
            temp_image[i][j].rgbtGreen = (int) round(sum_g / counter);
            temp_image[i][j].rgbtBlue = (int) round(sum_b / counter);
        }
    }
    
    //assign the values in temp array to the original image array.
    for (int k = 0; k < height; k ++)
    {
        for (int l = 0; l < width; l++)
        {
            image[k][l] = temp_image[k][l];
        }
    }
    return;
}

// Detect edges
void edges(int height, int width, RGBTRIPLE image[height][width])
{
    // a temporary array to store the edited values
    RGBTRIPLE temp_image[height][width];
    // the values in the kernel
    int kernel[3][3];
    kernel[0][0] = -1;
    kernel[0][1] = 0;
    kernel[0][2] = 1;
    kernel[1][0] = -2;
    kernel[1][1] = 0;
    kernel[1][2] = 2;
    kernel[2][0] = -1; 
    kernel[2][1] = 0;
    kernel[2][2] = 1;
     
    for (int i = 0; i < height; i++)
    {
        for (int j = 0; j < width; j++)
        {   
            // the 6 values needed to calculate
            int gx_r = 0, gx_g = 0, gx_b = 0;
            int gy_r = 0, gy_g = 0, gy_b = 0;
            int red, green, blue;
            
            int x = 0;
            for (int m = i - 1; m <= i + 1; m++)
            {   
                int y = 0;
                // check for edges
                if ((m < 0) || (m > height - 1))
                {
                    x += 1;
                    continue;
                }
                for (int n = j - 1; n <= j + 1; n ++)
                {
                    //check for edges
                    if ((n < 0) || (n > width - 1))
                    {
                        y += 1;
                        continue;
                    }
                    gx_r += image[m][n].rgbtRed * kernel[x][y];
                    gx_g += image[m][n].rgbtGreen * kernel[x][y];
                    gx_b += image[m][n].rgbtBlue * kernel[x][y];
                    gy_r += image[m][n].rgbtRed * kernel[y][x];
                    gy_g += image[m][n].rgbtGreen * kernel[y][x];
                    gy_b += image[m][n].rgbtBlue * kernel[y][x];
                    y ++;
                }
                x ++;
            }
            //calculat the final value for each r g and b.
            red = check_interval(round(sqrt(gx_r * gx_r + gy_r * gy_r)));
            green = check_interval(round(sqrt(gx_g * gx_g + gy_g * gy_g)));
            blue = check_interval(round(sqrt(gx_b * gx_b + gy_b * gy_b)));
            temp_image[i][j].rgbtRed = red;
            temp_image[i][j].rgbtGreen = green;
            temp_image[i][j].rgbtBlue = blue;
        }
    }
    
    // asssigns the values in the temp. array to the original image array.
    for (int k = 0; k < height; k ++)
    {
        for (int l = 0; l < width; l++)
        {
            image[k][l] = temp_image[k][l];
        }
    }
    return;
}

// checks the interval for values that may be higher than 255.
int check_interval(int x)
{
    if (x > 255)
    {
        x = 255;
    }
    return x;
}